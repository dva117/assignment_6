#define _CRT_SECURE_NO_WARNINGS
#include"ShoppingList.h"
#include<stdio.h>
#include<string.h>
#include<stdlib.h> // For malloc() and free()
#define F_LENGTH 32

//==============================================================================
void addItem(struct ShoppingList *list)
{
    if ((*list).length > 4)
    {
        printf("\nThe shopping list is full");
    }

    else
    {
        printf("Enter name: ");
        scanf("%s", (*list).itemList[(*list).length].productName);			
        do
        {
            printf("Enter amount: ");
            scanf("%f", &(*list).itemList[(*list).length].amount);
        } while ((*list).itemList[(*list).length].amount < 0);
        printf("Enter unit: ");
        scanf("%s", (*list).itemList[(*list).length].unit);
        printf("\n%s was added to the shopping list", (*list).itemList[(*list).length].productName);
        (*list).length++;
    }
}
//==============================================================================
void printList(struct ShoppingList *list)
{
    int i;

    if ((*list).length < 1)
    {
        printf("\nYour shopping list is empty");
    }

    else
    {
        printf("\nShopping list: \n");
        for(i = 0; i < (*list).length; i++)
        {
            printf("% 3d. %-20s % 5.2f %s\n", i+1, (*list).itemList[i].productName, (*list).itemList[i].amount, (*list).itemList[i].unit);
        }
    }
}
//==============================================================================
void editItem(struct ShoppingList *list)
{
    int item;

    if ((*list).length <= 0)
    {
        printf("\nYour shopping list doesn't contain any items");    
    }

    else
    {
        printList(list);    
        printf("\nWhich item do you wish to change?: ");
        scanf("%d", &item);
        item = item - 1;

        if ((item >= (*list).length) || (item < 0))
        {
            printf("\nYour list only contains %d items", (*list).length);
        }
        else
        {
            printf("\nCurrent amount: %.2f %s", (*list).itemList[item].amount, (*list).itemList[item].unit);
            printf("\nEnter new amount: ");        
            scanf("%f", &(*list).itemList[item].amount);
        }
    }
}
//==============================================================================
void removeItem(struct ShoppingList *list)
{
    int i;
    int item;

    if ((*list).length <= 0)
    {
        printf("\nYour shopping list doesn't contain any items");
    }

    else
    {
        printList(list); 
        printf("\nWhich item do you wish to remove?: ");
        scanf("%d", &item);
        item = item - 1;

        if ((item >= (*list).length) || (item < 0))
        {
            printf("\nYour list only contains %d items", (*list).length);
        }

        else
        {
            printf("\n%s has been removed from the shopping list", (*list).itemList[item].productName);
            for (i = item; i < (*list).length; i++)
            {
                strcpy((*list).itemList[i].productName, (*list).itemList[i+1].productName);
                (*list).itemList[i].amount = (*list).itemList[i+1].amount;
                strcpy((*list).itemList[i].unit, (*list).itemList[i+1].unit);
            }
            (*list).length--;
        }
    }
}
//==============================================================================
void saveList(struct ShoppingList *list)
{
    char save_file[F_LENGTH];
    FILE *fp;
    int i;

    printf("\nEnter save file name('filename.txt'): ");
    scanf("%s", save_file);
    fp = fopen(save_file, "w+");

    if (fp != NULL)
    {
        for(i = 0; i < (*list).length; i++)
        {
            fprintf(fp, "%s %f %s\n", (*list).itemList[i].productName, (*list).itemList[i].amount, (*list).itemList[i].unit);
        }
        fclose(fp);
        printf("\nList saved to file %s", save_file);
    }

    else
    {
        printf("\nUnable to save to file %s", save_file);
    }
}
//==============================================================================
void loadList(struct ShoppingList* list)
{
    char load_file[F_LENGTH];
    FILE *fp;
    int i = 0;

    printf("\nEnter load file name: ");
    scanf("%s", load_file);
    fp = fopen(load_file, "r");

    if (fp != NULL)
    {
        while(fscanf(fp, "%s %f %s", (*list).itemList[i].productName, &(*list).itemList[i].amount, (*list).itemList[i].unit) > 0)
        {
            i++;
        }
        (*list).length = i;
        fclose(fp);
        printf("\nFile %s has been loaded", load_file);
    }

    else
    {
        printf("\nUnable to load file %s", load_file);
    }
}
